package br.com.fastshop.generator;

import br.com.fastshop.generator.service.GeneratorService;
import br.com.fastshop.generator.vo.DadosParametro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class CodeGeneratorApplication implements ApplicationRunner {

	@Autowired
	private GeneratorService generatorService;

	private DadosParametro dadosParametro;

	public static void main(String[] args) {

		SpringApplication.run(CodeGeneratorApplication.class, args);

	}

	@Override
	public void run(ApplicationArguments applicationArguments) throws Exception {

		String[] argsParan = applicationArguments.getSourceArgs();

		DadosParametro dadosParametro = new DadosParametro();

		dadosParametro.setNameProjeto(argsParan[0]);
		dadosParametro.setNameNegocio(argsParan[1]);
		dadosParametro.setNameEntidade(argsParan[2]);
		dadosParametro.setNamePathDestiny(argsParan[3]);

		generatorService.processar(dadosParametro);
	}

	@Bean
	public DadosParametro getDadosParametro() {
		return new DadosParametro();
	}

}
