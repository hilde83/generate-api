package br.com.fastshop.generator.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.File;

@Getter
@Setter
public class DadosParametro {

    private String nameProjeto;

    private String nameNegocio;

    private String nameEntidade;

    private String namePathDestiny;

    public File destinyFile(){

        File path = new File(this.namePathDestiny);

        return path;
    }

}
