package br.com.fastshop.generator.service;

import br.com.fastshop.generator.vo.DadosParametro;

public interface GeneratorService {

    public void processar(DadosParametro dadosParametro);
}
