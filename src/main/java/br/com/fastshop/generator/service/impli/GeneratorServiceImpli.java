package br.com.fastshop.generator.service.impli;

import br.com.fastshop.generator.vo.DadosParametro;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import br.com.fastshop.generator.service.GeneratorService;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

@Service
public class GeneratorServiceImpli implements GeneratorService {


    @Override
    public void processar(DadosParametro dadosParametro) {

        File file = nomeClass();

        copy(file,dadosParametro.destinyFile());

    }


    private void setTemplate(File file, DadosParametro dadosParametro) {

        Configuration cfg = new Configuration(new Version("2.3.23"));

        cfg.setClassForTemplateLoading(GeneratorServiceImpli.class, "/");
        cfg.setDefaultEncoding("UTF-8");

        Template template = null;
        try {
            template = cfg.getTemplate(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String, Object> templateData = new HashMap<>();
        templateData.put("dadosParametro", dadosParametro);

        try {
            try (StringWriter out = new StringWriter()) {

                template.process(templateData, out);
                System.out.println(out.getBuffer().toString());
                out.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }

    }

    private File nomeClass(){

        String pathResources = this.getClass().getResource("/static").toExternalForm();

        File path = new File(pathResources);

        return path;
    }

    public File[] listDir(File dir, DadosParametro dadosParametro) {
        Vector enc = new Vector();

        File[] files = dir.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                //Adiciona no Vector os arquivos encontrados dentro de 'files[i]':
                File[] recFiles = listDir(files[i], dadosParametro);
                for (int j = 0; j < recFiles.length; j++) {
                    setTemplate(recFiles[j], dadosParametro);
                }
            } else {
                //Adiciona no Vector o arquivo encontrado dentro de 'dir':
                setTemplate(files[i], dadosParametro);
            }
        }

        //Transforma um Vector em um File[]:
        File[] encontrados = new File[enc.size()];
        for (int i = 0; i < enc.size(); i++) {
            encontrados[i] = (File)enc.elementAt(i);
        }
        return encontrados;
    }

    public boolean copy( File srcDir, File dstDir ){

        try{

            if(srcDir.isDirectory()){

                if( !dstDir.exists()){

                    dstDir.mkdir();
                }

                String[] children = srcDir.list();

                for (int i=0; i<children.length; i++){

                    copy( new File( srcDir, children[i] ), new File( dstDir, children[i] ) );
                }
            }
            else{

                InputStream in = new FileInputStream( srcDir );
                OutputStream out = new FileOutputStream( dstDir );

                byte[] buf = new byte[1024];
                int len;

                while( (len = in.read( buf ) ) > 0 ) {

                    out.write( buf, 0, len );
                }

                in.close();
                out.close();
            }
        }
        catch( IOException ioex ){

            ioex.printStackTrace();
            return false;
        }

        return true;
    }
}
