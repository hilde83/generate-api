package br.com.empresa.projeto.facede;

import br.com.empresa.projeto.dto.EntidadeDefaultDto;

import java.util.List;

public interface NegocioFacede {

    void createOrUpdate(EntidadeDefaultDto entidadeDefaultDto);

    EntidadeDefaultDto findById(Long id);

    void delete(Long id);

    List<EntidadeDefaultDto> listAll();
}
