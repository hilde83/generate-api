package br.com.empresa.projeto.facede.impli;

import br.com.empresa.projeto.dto.EntidadeDefaultDto;
import br.com.empresa.projeto.facede.NegocioFacede;
import br.com.empresa.projeto.model.EntidadeDefault;
import br.com.empresa.projeto.service.EntidadeDefaultService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class NegocioFacedeImpli implements NegocioFacede {

    @Autowired
    private EntidadeDefaultService entidadeDefaultService;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void createOrUpdate(EntidadeDefaultDto entidadeDefaultDto) {

        EntidadeDefault entidadeDefault = this.convertToEntity(entidadeDefaultDto);

        entidadeDefaultService.createOrUpdate(entidadeDefault);
    }

    @Override
    public EntidadeDefaultDto findById(Long id) {

        EntidadeDefault entidadeDefault = entidadeDefaultService.findById(id);

        EntidadeDefaultDto entidadeDefaultDto = this.convertToDto(entidadeDefault);

        return entidadeDefaultDto;
    }

    @Override
    public void delete(Long id) {

        entidadeDefaultService.delete(id);

    }

    @Override
    public List<EntidadeDefaultDto> listAll() {

        List<EntidadeDefault> entidadeDefaults = entidadeDefaultService.findAll();

        List<EntidadeDefaultDto> entidadeDefaultDtos = new ArrayList<>();

        for (EntidadeDefault entidadeDefault:entidadeDefaults) {

            EntidadeDefaultDto entidadeDefaultDto = convertToDto(entidadeDefault);

            entidadeDefaultDtos.add(entidadeDefaultDto);
        }


        return entidadeDefaultDtos;
    }

    private EntidadeDefaultDto convertToDto(EntidadeDefault entidadeDefault) {

        EntidadeDefaultDto entidadeDefaultDto = modelMapper.map(entidadeDefault, EntidadeDefaultDto.class);

        return entidadeDefaultDto;
    }

    private EntidadeDefault convertToEntity(EntidadeDefaultDto entidadeDefaultDto){

        EntidadeDefault entidadeDefault = modelMapper.map(entidadeDefaultDto, EntidadeDefault.class);

        return entidadeDefault;
    }
}
