package br.com.empresa.projeto.service;

import br.com.empresa.projeto.model.EntidadeDefault;

import java.util.List;

public interface EntidadeDefaultService {

    public EntidadeDefault createOrUpdate(EntidadeDefault entidadeDefault);

    public EntidadeDefault findById(Long id);

    public void delete(Long id);

    public List<EntidadeDefault> findAll();
}
