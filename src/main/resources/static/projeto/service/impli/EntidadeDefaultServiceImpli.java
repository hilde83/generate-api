package br.com.empresa.projeto.service.impli;

import br.com.empresa.projeto.model.EntidadeDefault;
import br.com.empresa.projeto.repository.EntidadeDefaultRepository;
import br.com.empresa.projeto.service.EntidadeDefaultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntidadeDefaultServiceImpli implements EntidadeDefaultService {

    @Autowired
    private EntidadeDefaultRepository entidadeDefaultRepository;

    public EntidadeDefault createOrUpdate(EntidadeDefault entidadeDefault) {
        return this.entidadeDefaultRepository.save(entidadeDefault);
    }

    public EntidadeDefault findById(Long id) {

       EntidadeDefault entidadeDefault = this.entidadeDefaultRepository.findById(id).get();

       return entidadeDefault;
    }

    @Override
    public void delete(Long id) {
        this.entidadeDefaultRepository.deleteById(id);
    }

    public List<EntidadeDefault> findAll() {

       List<EntidadeDefault> entidadeDefaults = this.entidadeDefaultRepository.findAll();

       return entidadeDefaults;
    }
}
