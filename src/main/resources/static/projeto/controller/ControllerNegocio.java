package br.com.empresa.projeto.controller;

import br.com.empresa.projeto.dto.EntidadeDefaultDto;
import br.com.empresa.projeto.facede.NegocioFacede;
import br.com.empresa.projeto.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/entidade")
@CrossOrigin(origins = "*")
public class ControllerNegocio {

    @Autowired
    private NegocioFacede negocioFacede;

    @GetMapping(value = "{id}")
    public ResponseEntity<Response<EntidadeDefaultDto>> findById(@PathVariable("id") Long id, BindingResult result) {

        Response<EntidadeDefaultDto> response = new Response<EntidadeDefaultDto>();

        EntidadeDefaultDto entidadeDefaultDto = negocioFacede.findById(id);

        if (entidadeDefaultDto == null) {
            response.getErrors().add("Register not found id:" + id);
            return ResponseEntity.badRequest().body(response);
        }

        response.setData(entidadeDefaultDto);

        return ResponseEntity.ok(response);

    }

    @PostMapping
    public ResponseEntity<Response<EntidadeDefaultDto>> create(@RequestBody EntidadeDefaultDto entidadeDefault, BindingResult result) {

        Response<EntidadeDefaultDto> response = new Response<EntidadeDefaultDto>();

        try {

            validate(entidadeDefault, result);

            if (result.hasErrors()) {
                result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
                return ResponseEntity.badRequest().body(response);
            }

            negocioFacede.createOrUpdate(entidadeDefault);

        } catch (DuplicateKeyException dE) {
            response.getErrors().add("E-mail already registered !");
            return ResponseEntity.badRequest().body(response);
        } catch (Exception e) {
            response.getErrors().add(e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
        return ResponseEntity.ok(response);

    }
    @PutMapping
    public ResponseEntity<Response<EntidadeDefaultDto>> update(@RequestBody EntidadeDefaultDto entidadeDefault, BindingResult result) {

        Response<EntidadeDefaultDto> response = new Response<EntidadeDefaultDto>();

        try {
            validate(entidadeDefault, result);
            if (result.hasErrors()) {
                result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
                return ResponseEntity.badRequest().body(response);
            }

            negocioFacede.createOrUpdate(entidadeDefault);

            response.setData(entidadeDefault);
        } catch (Exception e) {
            response.getErrors().add(e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }

        return ResponseEntity.ok(response);
    }

    @DeleteMapping
    public ResponseEntity<Response<String>> delete(@PathVariable("id") Long id, BindingResult result) {

        Response<String> response = new Response<String>();

        try {
            validate(id, result);
            if (result.hasErrors()) {
                result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
                return ResponseEntity.badRequest().body(response);
            }

            negocioFacede.delete(id);

        } catch (Exception e) {

            response.getErrors().add(e.getMessage());

            return ResponseEntity.badRequest().body(response);
        }

        return ResponseEntity.ok(new Response<String>());
    }


    private void validate(Object object, BindingResult result) {
        if (object == null) {
            result.addError(new ObjectError("User", "Valida Dados"));
            return;
        }
    }


}
