package br.com.empresa.projeto.repository;

import br.com.empresa.projeto.model.EntidadeDefault;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface EntidadeDefaultRepository extends MongoRepository<EntidadeDefault, Long> {
}
